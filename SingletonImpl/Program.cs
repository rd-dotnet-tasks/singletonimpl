﻿using System;

namespace SingletonImpl
{
    class Program
    {
        static void Main(string[] args)
        {
            Admin admin = new Admin();
            Manager manager = new Manager();
            admin.addUser();
            manager.addUser();
            admin.getUser();
            Singleton singletonImpl = Singleton.getInstance();
            Console.WriteLine("Number of instances of Singleton = " + singletonImpl.getCount);
        }
    }
}
