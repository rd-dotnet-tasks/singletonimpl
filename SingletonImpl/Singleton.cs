﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonImpl
{
    class User
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
    }

    sealed class Singleton
    {
        private static readonly Singleton _instance = new Singleton();
        private static int counter;
        private List<User> _list;
        private Singleton()
        {
            _list = new List<User>();
            counter++;
        }

        public int getCount
        {
            get { return counter; }
        }
        public void addUser(int id, string name, string email)
        {
            _list.Add(new User { Id = id, Name = name, Email = email });
        }

        public User getUser(int id)
        {
            return _list.Find(user => user.Id == id);
        }

        public static Singleton getInstance()
        {
            return _instance;
        }
    }
}
