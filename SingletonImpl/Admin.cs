﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonImpl
{
    class Admin
    {
        Singleton adminSingletonImpl;
        private int _id;
        private string _name, _email;
        public Admin()
        {
            adminSingletonImpl = Singleton.getInstance();
        }

        public void addUser()
        {
            Console.WriteLine("Enter id, name and email id of the user");
            Int32.TryParse(Console.ReadLine(), out _id);
            _name = Console.ReadLine();
            _email = Console.ReadLine(); ;
            adminSingletonImpl.addUser(_id, _name, _email);
        }
        public void getUser()
        {
            Console.WriteLine("Enter id of the user");
            Int32.TryParse(Console.ReadLine(), out _id);
            var user = adminSingletonImpl.getUser(_id);
            Console.WriteLine($"{user.Name} {user.Email}");
        }
    }
}
